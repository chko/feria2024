function CTurnPanel(iX,iY,oParentContainer){
    var _oDisc;
    var _oText;
    var _oContainer;
    var _oParentContainer;
    
    this._init = function(iX,iY){
        _oContainer = new createjs.Container();
        _oContainer.x = iX;
        _oContainer.y = iY;
        _oParentContainer.addChild(_oContainer);
        
        var oSpriteBg = s_oSpriteLibrary.getSprite("bg_turn");
        var oBg = createBitmap(oSpriteBg);
        _oContainer.addChild(oBg);
        
        var szText;
        if(s_iCurMode === MODE_CPU){
            szText = TEXT_PLAYER_TURN;
        }else{
            szText = TEXT_PLAYER1_TURN;
        }
        
        _oText = new CTLText(_oContainer, 
                    22, 14, 138, 30, 
                    20, "center", "#fff", PRIMARY_FONT, 1,
                    0, 0,
                    szText,
                    true, true, true,
                    false );

        
        _oDisc = new CDisc({x:oSpriteBg.width/2,y:oSpriteBg.height/2 + 7},s_oGame.getCurTurn(),_oContainer);
    };
    
    this.setTurn = function(iTurn,szTurnText){
        _oText.text = szTurnText;
        _oDisc.setFrame(iTurn);
    };
    
    _oParentContainer = oParentContainer;
    this._init(iX,iY);
}