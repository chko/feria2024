var TEXT_PRELOADER_CONTINUE = "JUGAR";
var TEXT_SCORE = "TOTAL: ";
var TEXT_BONUS_TIME = "BONUS TIEMPO";
var TEXT_BONUS_WIN = "BONUS VICTORIA";
var TEXT_YOU_LOSE = "PERDISTE";
var TEXT_CONGRATS = "FELICIDADES";
var TEXT_RED_WINS = "VICTORIA!!";
var TEXT_YELLOW_WINS = "VICTORIA!!";
var TEXT_PLAYER_TURN = "TURNO";
var TEXT_CPU_TURN = "CPU TURN";
var TEXT_PLAYER1_TURN = "TURNO";
var TEXT_PLAYER2_TURN = "TURNO";
var TEXT_TIE = "EMPATE!!";
var TEXT_CHOOSE_MODE = "ELIGE TU MODO DE JUEGO";
var TEXT_ARE_YOU_SURE = "SEGURO?";

var TEXT_DEVELOPED = "developed by";

var TEXT_SHARE_IMAGE = "200x200.jpg";
var TEXT_SHARE_TITLE = "Felicidades!";
var TEXT_SHARE_MSG1 = "You collected <strong>";
var TEXT_SHARE_MSG2 = " points</strong>!<br><br>Share your score with your friends!";
var TEXT_SHARE_SHARE1 = "My score is ";
var TEXT_SHARE_SHARE2 = " points! Can you do better";
